"""
    Digital backing of the looping paper compiling exercises for
    Carlow Python intro course 23FA

"""


def time_travel():
    print("Time travel")
    PRESENT = 2023
    travel_year = 2053
    jump = True
    print(travel_year, jump)
    while travel_year > PRESENT:
        if jump:
            travel_year = travel_year - 10
            print("Hop!")
        else:
            travel_year = travel_year + 3
        jump = not jump
        print(travel_year)
    print(travel_year, jump)


def gigawatts():
    print("Gigawatts")
    zap = 50
    current = 12
    voltage = 110
    print(current, zap, voltage)
    while current:  # using truthiness to control the while; b/c current is int,
                    # this expression will be True when current is not zero
        if current < 5 or current > 100:
            print("Compound logic")
        else:
            voltage = voltage + (zap * 2)
            if zap:
                print("Nonzero zap", zap)
                zap = zap - 25
        current = current - 2
    print(current, zap, voltage)


def move():
    print("Move")
    hover = '~'
    train = 6
    delorean = 88
    while train < delorean:
        hover = hover + '&' + str(delorean)
        if not delorean:
            print(hover, end='')
            if hover == '88':
                continue
        else:
            hover = hover + 'K'
            delorean = delorean - 22
            print(delorean)
        if len(hover) >= 7:
            break
        print("decelerate!")
    print(hover)


divider = '---------------'
if __name__ == '__main__':
    time_travel()
    print(divider)
    gigawatts()
    print(divider)
    move()




