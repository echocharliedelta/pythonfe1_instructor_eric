"""
In-class demo of the random module
is in the standard library,
documentation at python.org
"""
import random as rand   # we give the module random
                        # our own variable name
import time
# simplest possible float draw
random_float = rand.random()
print(random_float)
# draw randomly between 0 and 6000
random_int = rand.randrange(6001)
print(random_int)
# if we don't want zero as our lower bound
# we ca use overloaded randrange and give
# lower, upper, and step
rand_from_range = rand.randrange(2000, 3001)
print(rand_from_range)
# combine with while to visualize the magnitude of rand draws
max_lines = 1000
counter = 0
while counter < max_lines:
    # randomly draw an int from 0 to 80 (std line length)
    rnd = rand.randrange(81)  # upper bound is exclusive
    # print the random numbers worth of # signs on a line
    inner_counter = 0
    while inner_counter <= rnd:
        print('#', end='')  # don't go to the next line
        inner_counter = inner_counter + 1
    time.sleep(.25)  # pause for a quarter second
    print('')  # start a new line and go back to top of while
    counter = counter + 1
