"""
    Sample code to accompany a lesson on looping essentials taught at Carlow University
    Fall 2023 by Eric Darsow via technology rediscovery dot net.
    https://technologyrediscovery.net/python/everyone/looping.html
    Copyright: GPL v.3
"""

import random as rand

# Final vars
LADY = "Lady on a red bike"
MAN = "Man with flowers"
BEETLE = "Volkswagon beetle with a dented fender"
SEP = "--------------------"


def trumanloop():
    print("How many times did Truman watch the cycle?")
    numloops = int(input())
    counter = 0
    while counter <= numloops:
        print("Loop: ", counter)
        print(LADY)
        print(MAN)
        print(BEETLE)
        inner_count = 1
        counter = counter + 1
        while inner_count < counter:
            print(inner_count, end=" ")
            inner_count = inner_count + 1
        print()
        print(SEP)
    print("Done looping; exiting.")


def looping_art_1():
    print("Pyramid: 10 total rows, Each row displays one \nadditional character until max rows is reached")
    cols = 10
    rows = 10
    char = '*'
    col_count = 0
    row_count = 0
    while row_count < rows:
        while col_count <= cols:
            cursor = 0
            while cursor < col_count:
                print(char, end='')
                cursor = cursor + 1
            col_count = col_count + 1
            print()
        row_count = row_count + 1


def looping_art_2():
    print("How many rows/columns shall I print")
    row_col_request = int(input())
    print("Enter the even numbered character to print:")
    char_even = input()
    print("Enter the odd numbered character to print:")
    char_cdd = input()
    print("How many spaces shall I insert between characters?")
    padding_count = int(input())
    # create padding character from padding_count
    padding = ''
    padding_control = 0
    while padding_control <= padding_count:
        padding = padding + ' '  # add a blank space to the end for each count of padding_count
        padding_control = padding_control + 1
    # setup counters for our looping
    col_count = 0
    row_count = 0
    while row_count < row_col_request:
        while col_count <= row_col_request:
            cursor = 0
            while cursor < col_count:
                if cursor % 2 == 0:
                    print(char_even, end='')
                else:
                    print(char_cdd, end='')
                print(padding, end='')
                cursor = cursor + 1
            col_count = col_count + 1
            print()
        row_count = row_count + 1


def convergence():
    MAX_GROUND_SPEED_MPH = 392.0
    print("Enter the secred speed in miles per hour (groundspeed) of the DeLorean ")
    print("which the computer will then try to guess:")
    target_speed = float(input())
    print("How close does the computer's guess need to be to count as matched (also in miles per hour)?")
    tolerance = float(input())
    # setup a control structure for our guessing
    guess_count = 0
    guess_on_target = False
    while not guess_on_target:
        # choose a value between 0 and 1
        rand_float = rand.random()
        # generate a value in your context's range
        computer_guess = MAX_GROUND_SPEED_MPH * rand_float
        distance_from_target = abs(target_speed - computer_guess)
        guess_count = guess_count + 1
        print("Guess", guess_count, ":", computer_guess)
        if distance_from_target <= tolerance:
            print("**************************")
            print("Target speed reached! Computer guess:", computer_guess, "mph")
            print("which is", distance_from_target, "mph from the target of", target_speed)
            print("Total guesses required:", guess_count)
            print("**************************")
            break
    print("end of guessing")





if __name__ == '__main__':
    # trumanloop()
    # looping_art_1()
    # looping_art_2()
    convergence()