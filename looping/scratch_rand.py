"""
Tinker code pertaining to the random module
"""
import random as rand

# draw me a float between 0 and 1
rfloat = rand.random()
print(rfloat)
# use this to choose from within a range of possible numbers to choose
# for now, let's choose a range that starts at zero, as in not moving
# and the upper end would be a land speed record: 390 mph
MAX_SPEED = 394.0
computer_guess = rfloat * MAX_SPEED
print("Computer guess: ", computer_guess, "mph")
