"""
Looping demo: iteration with while
Eric Darsow
In-class copy; Licensed under GPL v.3
"""

# Imitate Truman watching his world in a loop
counter = 0  # store the current loop's number
print("How many cycles?")
max_loops = int(input())  # upper limit of looping
cutoff_threshold = 50000
while counter <= max_loops:
    print("Counter: ", counter)  # watch our iteration
    print("Lady on a red bike")  # from clip
    print("Man with flowers")
    # demo continue: skip the beetle on even numbered iterations
    if counter % 2 == 0:   # when we divide counter by 2 and get a nonzero output, print beetle
        print("Volkswagon beetle with a dented fender")
    print("--------------")
    # prevent memory overrun
    counter = counter + 1  # incrementing counter at end of interation
    if counter >= cutoff_threshold:
        print("too many loops!")
        break
print("end of loop")
