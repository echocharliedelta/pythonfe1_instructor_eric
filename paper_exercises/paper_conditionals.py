"""
Practice exercise for conditional execution in Python
"""


def swim():
    print("Call Swim")
    breath_sec = 45
    dunks = 3
    laps = 14
    flotation = True
    print(breath_sec, dunks, laps, flotation)
    if dunks <= laps:
        dunks = dunks + 3
    else:
        breath_sec = breath_sec + 10
    if flotation:
        breath_sec = breath_sec - 2
        laps = laps - 4
        if dunks is not laps:
            flotation = not flotation
    else:
        laps = laps + 8
    breath_sec = breath_sec + 15
    print(breath_sec, dunks, laps, flotation)


def reggae():
    print("Call reggae")
    origin = 'Jamaica'
    year = 1968
    length = 190
    if 1940 <= year <= 1949:
        print("World war")
    elif 1950 <= year <= 1959:
        print("Postwar boom")
    elif 1960 <= year <= 1969:
        print("The turbulent 60s")
    elif 1970 <= year <= 1979:
        print("Exploration in the 70s")
    else:
        year = year + 2
        print("Decade unknown")
    if length > 180 and year != 1964:
        length = length + 25
        if origin == 'USA':
            length = length + 75
    elif length < 250:
        print("Too long")
    else:
        print("No compound match")
    if length > 200 or year < 1980:
        print("Final if")
        length = length + 5
    print(origin, year, length)


def dinner():
    print("Call dinner")
    protein = "Fish"
    temp = 450
    duration = 45
    if protein == "Fish" or duration > 30:
        print("Ocean soup")
    if duration >= 45:
        print("Too long for fish")
        duration = duration - 30
        protein = "Tripe"
        print(protein, temp, duration)
    if 10 < duration < 20 and temp > 440:
        protein = "Beef"
        duration = duration + 8
        if temp > 500:
            print("We're burning up in here.")
        else:
            print("Temp OK")
            if protein == "Beef":
                print("Moo!")
    print(protein, temp, duration)


def drive():
    print("Call drive")
    vehicle = "Limo"
    mileage = 320000
    trip = 3000
    delta_oil = 1800
    change_filter = False
    if (mileage > 300000 and delta_oil > 1500) or vehicle == "Truck":
        print("Early change")
        change_filter = not change_filter
    elif 150000 < mileage < 299999 and delta_oil > 3000:
        print("On time change")
    else:
        print("Beats me; Follow book")
    mileage = mileage + trip
    delta_oil = ((trip / 100) * 2) + delta_oil
    if delta_oil > 6500 or mileage > 310000:
        change_filter = not not change_filter
    if change_filter:
        print("Swap can, too")
    print(vehicle, mileage, delta_oil)


if __name__ == '__main__':
    swim()
    print("********")
    reggae()
    print("********")
    dinner()
    print("********")
    drive()