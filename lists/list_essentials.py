"""
Essetial python list data structures
"""


def main():
    print("List operations: Software design steps")
    # create a list with literal values
    trumans_world = ["Lady", "Flowers", "Beetle"]
    print("A loop shattered Truman's world:", trumans_world)
    # more often we create lists programatically
    steps = []  # create an empty list
    steps.append("Study specs")
    steps.append("Diagram")
    steps.append("Code component")
    steps.append("Test component")
    steps.append("...Code then test other components")
    steps.append("Check against specs")
    # print will format list to get it on the screen
    print(steps)
    print("We can remove items, such as 'Test component', and: disaster ensues!")
    steps.remove("Test component")
    print(steps)
    # we can swap the order of items easily
    steps.reverse()
    print("And the inverted process is less logical:")
    print(steps)


def indexing():
    zero_indexed = ["First item", "Second item", "Third item"]
    print("Our list has three items, indexed 0, 1, and 2:")
    print(zero_indexed)
    retrieved_item = zero_indexed[1]
    print("See, when we acces the item with index of 1, we get the SECOND list item:", retrieved_item)
    print("So be careful to not access out of bounds such as by asking for the item at index 3:")
    retrieved_item = zero_indexed[3]
    print(retrieved_item)


if __name__ == '__main__':
    # main()
    indexing()

