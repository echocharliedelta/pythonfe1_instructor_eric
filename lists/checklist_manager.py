"""
Sample program to meet list project specification
"""


def manage_checklist():
    # setup list
    checklist = []
    while True:
        choice = display_menu_get_choice()
        if choice == 1:
            print("*********** CHECKLIST ***********")
            for item in checklist:
                print(checklist.index(item), ":", item)
            print("Number of items:", len(checklist))
            print("*********************************")
        elif choice == 2:
            print("Type the name of the checklist item and press enter:")
            new_item = input()
            checklist.append(new_item)
            print("Success: Added", new_item, "to your checklist!")
        elif choice == 3:
            print("Which item do you want to remove? Type the name exactly as it appears and press Enter:")
            to_remove = input()
            try:
                checklist.remove(to_remove)
                print("Removed", to_remove, "from checklist")
            except ValueError:
                print("Could not find item", to_remove, "so it could not be removed")
        elif choice == 4:
            print("Enter the number of the item to remove and press enter:")
            idx_to_remove = int(input())
            if -1 < idx_to_remove <= len(checklist):
                del checklist[idx_to_remove]
                print("Success: removed item", idx_to_remove)
            else:
                print("Removal index out of bounds")
        elif choice == 5:
            print("Goodbye Dave.")
            break
        else:
            print("Unknown action; Please try again.")


def display_menu_get_choice():
    print("Enter the number corresponding to your desired action and press Enter")
    print("1: View checklist")
    print("2: Add item to end of checklist")
    print("3: Remove item from checklist by name")
    print("4: Remove item from checklist by position")
    print("5: End program")
    return int(input())


if __name__ == '__main__':
    manage_checklist()

