# Programming for Everyone: Python 1 at Carlow Univesrity

An introduction to programming essentials. First run: Fall 2023. The course's content is produced under the philosophy of Eric Darsow's container LLC:

!["Technology Rediscovery"](https://technologyrediscovery.net/img/logo/T_R_FullLogo_Color.png)

## Instructor code

This repository contains exercises and project helper code for the CSC-101 taught by Eric Darsow during the fall of 2023 at Carlow Univesrity in Pittsburgh, PA. [Companion website index is open access on technologyrediscovery.net](https://technologyrediscovery.net/#csc-101). The [course scheudle](https://technologyrediscovery.net/python/everyone/csc101_schedule_23FA.html) holds the master sequence.

## Might we be friends?
A project for practicing conditional logic and variable creation and assignment. The [project guide](https://technologyrediscovery.net/python/everyone/friends/mightWeBeFriendspy.html) provides full program specifications.


