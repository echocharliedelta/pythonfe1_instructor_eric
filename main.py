# This is a sample Python script.

def print_hi(name):
    class_responses = []
    lines = 0
    while lines != -9:
        print("how many lines of code did you write this summer?")
        lines = int(input())
        class_responses.append(lines)
    print(class_responses)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
