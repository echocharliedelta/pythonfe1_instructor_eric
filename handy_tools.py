"""
 Handy python tools such as use of a random generator, timer, and more
"""

import time
import random as rand


def delay():
    print("Demo of pausing program execution")
    print("Don't forget to import the time module!")
    counter = 0
    while counter <= 3:
        print("Wait")
        time.sleep(1.4)  # delay in seconds
        print("go")
        counter += 1


def stochastic():
    print("Demo of using the random module to create variation")
    print("Don't forget to import the random module")
    # draw me a float between 0 and 1
    rfloat = rand.random()
    print(rfloat)
    # use this to choose from within a range of possible numbers to choose
    # for now, let's choose a range that starts at zero, as in not moving
    # and the upper end would be a land speed record: 390 mph
    MAX_SPEED = 394.0
    computer_guess = rfloat * MAX_SPEED
    print("Computer guess: ", computer_guess, "mph")
    


if __name__ == '__main__':
    delay()

