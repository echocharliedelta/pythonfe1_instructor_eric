"""
Practicing conditinal expressions
Eric Darsow; Fall 2023
"""

# Boolean expression evaluates to true or false
# Simplest is a literal boolean value: True, False
bol = False # this variable has a boolean value
# once we have a boolean value, we can control execution with if
if bol:
    print("If expression evaluates to True")
else:
    print("If expression is False")
print("********* AIRPLANE ALTITUDE CHECK *************")

# usally we're evaluating a more complex expression for true/false
plane1 = 4850
plane2 = 1800
# check for equality between two variables
# noinspection PyChainedComparisons
if plane1 == plane2:
    print("Collision immediate Danger--Exact same altitude!")
elif plane1 - plane2 < 3000:
    print("Planes too close; plane1 increase altitude; plane 2; decrease")
# You can chain as many elif's as you'd like
elif plane1 - plane2 < 4000 and plane1 - plane2 > 3000:
    print("TCAS Warning: Watch for traffic")
    # plan who should climb and who should descend with nested logic
    if plane1 - plane2 > 0:
        print("Plane 1 altitude higher, so plane 1 should climb; 2 should descend")
    else:
        print("Plane 2 altitude is higher, so plane 2 should climb, 2 should descend")
# Executed when all above checks are false
else:
    print("No immediate danger")
print("End of program")
