"""
    Author: Eric Darsow; Fall 2023; Carlow Univesrity
    Helper code for Might We Be Friends? Project
    https://technologyrediscovery.net/python/everyone/friends/mightWeBeFriendspy.html
    Licensed under the GPL version 3
    https://www.gnu.org/licenses/gpl-3.0.en.html
"""


def main():
    # Basic introductory code
    print("Friendships can be based on shared interests. The following program will explore interest overlaps with Eric.")
    print("What is your name?")
    candidate = input()
    print("Greetings", candidate, "We'll start with a few simple questions:")
    # Compatibility score variable initialized to zero
    score = 0
    print("Question 1: Rate your enthusiasm for air travel on, 1=refusal to engage, 10=pursuing pilot license")
    q1_air = int(input())
    # weight air travel interest by a factor of 2
    if q1_air <= 3:
        print("Oh, I see. No planes for you.")
    elif q1_air >= 4 and q1_air <=7:
        print("Okay, planes seem okay but not a major focus")
    else:
        print("Hot dog! Let's go flying!")
    # update my overall score by adding 2 times the air travel score to the original value of score
    score = score + (q1_air * 2)
    print("Your raw compatbility score is:", score)
    # now compute a percentage of possible points
    MAX_PTS = 20  # 2 times the most enthusiastic flying enthusiasm score
    perc_possible = (score / MAX_PTS) * 100
    # see https://docs.python.org/3/library/string.html
    print("Your scaled compatibility is {:03.2f} percent".format(perc_possible))
    # print("Your scaled compatibility is ",perc_possible, "percent")
    # Then use a final conditional to output friend likelihood
    if perc_possible >= 80.0:
        print("Okay, I think we could explore friendship!")
    else:
        print("Not likely to have enough overlap for friendship")


def stringinput():
    print("Type the model of the airplane Sully landed in the Hudson:")
    plane = input()
    if plane == "A320":
        print("correct!")
    else:
        print("nope!")


if __name__ == '__main__':
    # stringinput()
    main()

